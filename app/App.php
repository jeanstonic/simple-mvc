<?php


namespace app;


class App
{
    public function run()
    {
        if (count($_GET) == 0) {
            die ("type request domain.com?student=&lt;id&gt;");
        }
        $keys = array_keys($_GET);
        $controllerName = 'controllers\\' . ucfirst($keys[0]) . 'Controller';
        try {
            $controller = new $controllerName();
            $controller->defaultAction($_GET[$keys[0]]);
        } catch (\Exception $e){
            die ($e->getMessage());
        }
    }
}