<?php


namespace models;


class Student
{
    private $name;
    private $id;

    /**
     * @param $id
     * @return Student|null
     * @throws \Exception
     */
    public static function find($id)
    {
        if ($id <= 0 || $id > 10) {
            return null;
        }
        $student = new static();
        $student->id = $id;
        $student->name = "name-" . $id;
        return $student;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
}