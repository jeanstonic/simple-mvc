<?php


namespace controllers;


use models\Student;
use views\student\Json;

class StudentController
{
    /**
     * @param int $id
     * @throws \Exception
     */
    public function defaultAction($id = 0)
    {
        if ($id === 0) {
            throw new \Exception('incorrect id');
        }
        if (!$student = Student::find($id)) {
            throw new \Exception("Student with such id '$id do not exists");
        }
        Json::view($student);
    }
}