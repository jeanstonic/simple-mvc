<?php


namespace views\student;


use models\Student;

class Json
{
    public static function view(Student $student)
    {
        header('Content-Type: application/json');
        echo json_encode([
            'id' => $student->getId(),
            'name' => $student->getName()
        ]);
    }
}